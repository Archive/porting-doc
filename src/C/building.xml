<!-- vim: set ai et sw=3 ts=3 tw=80: -->
<sect1 id="build-environment">
   <title id="build-title">Changes to the build environment</title>

   <para>In general, the build setup for a GNOME 2 application will remain very
   similar to the setup for GNOME 1. The only changes that are required are due
   to newer versions of the tools like <application>autoconf</application> and
   to handle the inclusion of <application>pkg-config</application>, instead of
   library-specific scripts like <application>gnome-config</application>.
   </para>

   <sect2 id="autogen">
      <title id="autogen-title">Changes to <filename>autogen.sh</filename>
      </title>

      <para>The only change that you may wish to consider here is to make your
      package's <filename>autogen.sh</filename> script call the
      <filename>gnome-autogen.sh</filename> script with the
      <envar>USE_GNOME2_MACROS</envar> environment variable set. For example,
      see the <xref linkend="autogen-example"/> script below.  In general, it's
      fairly safe to just copy the <filename>autogen.sh</filename> from
      somewhere like the <application>gnome-hello</application> modules and
      just modify the package name. </para>

      <example id="autogen-example" xreflabel="sample autogen.sh">
         <title>Sample <filename>autogen.sh</filename></title>
         <programlisting>
#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" &amp;&amp; srcdir=.

PKG_NAME="Gnome Hello Demonstration Application"

(test -f $srcdir/configure.in \
  &amp;&amp; test -f $srcdir/ChangeLog \
  &amp;&amp; test -d $srcdir/src) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

which gnome-autogen.sh || {
    echo "You need to install gnome-common from the GNOME CVS"
    exit 1
}
USE_GNOME2_MACROS=1 . gnome-autogen.sh
         </programlisting>
      </example>
   </sect2>

   <sect2 id="pkg-config">
      <title id="pkg-config-title"><application>Pkg-config</application></title>

      <para>As mentioned in the short description of <xref
      linkend="pkg-config-desc"/>, this is the new way to do things that were
      previously done with calls to application specific scripts like
      <filename>gnome-config</filename>.</para>

      <para>You can list all of the libraries that are under
      <application>pkg-config</application> management by running the command
      <command>pkg-config --list-all</command>. If you do this, you'll see that
      all of the libraries that are part of the GNOME 2 platform are known to
      <application>pkg-config</application>.</para>

      <para>In almost all cases, it is sufficient to find the platform
      libraries that are the <emphasis>highest</emphasis> in the tree of
      dependencies.  That is, the library whose dependencies includes all of
      the other libraries you will need. Usually,
      <constant>libgnomeui-2.0</constant> is sufficient, since it's
      dependencies include <constant>gtk+</constant>,
      <constant>gconf</constant> and <constant>libbonoboui</constant> amongst
      others. Have a look at the <xref linkend="dependencies"
      endterm="dependencies-title"/> appendix for a graphic showing all of the
      dependencies between various modules. </para>
      
      <para>If you think you need something more specific or more complex
      that this, have a look at the <application>pkg-config</application>
      manual page for more information.</para>

      <para>For using packages that are managed by
      <application>pkg-config</application>, all of the changes are in
      <filename>configure.in</filename> which is covered in the section about
      <xref linkend="configure.in-changes"/>. If you are looking to make your
      own package usable via <application>pkg-config</application>, then
      you need to create an appropriate <filename>foo.pc</filename> file (if
      your package is indeed called <application>foo</application>).</para>

      <para>Typically, you would not create <filename>foo.pc</filename>
      directly, since it contains information about the installed location of
      the components of <application>foo</application>. Rather, you would create
      <filename>foo.pc.in</filename> with appropriate variables that can then
      be filled in from <filename>configure.in</filename>. By way of example,
      here is a slight variation on the <filename>gobject.pc.in</filename>
      file (the <literal>Conflicts</literal> line is not normal and there are
      some extra comments). This file would then be turned into
      <filename>gobject.pc</filename> via an appropriate line in the
      <systemitem class="macro">AC_OUTPUT</systemitem> section of
      <filename>configure.in</filename>.
      </para>

      <example><title>sample <filename>gobject.pc.in</filename> file</title>
         <programlisting>
# This is a comment
prefix=@prefix@
exec_prefix=@exec_prefix@
libdir=@libdir@
includedir=@includedir@

Name: GObject                            # human-readable name
Description: Object/type system for GLib # human-readable description
Version: @VERSION@
Requires: glib-2.0 = 1.3.1
Conflicts: foobar &lt;= 4.5
Libs: -L${libdir} -lgobject-1.3
Cflags: -I${includedir}/glib-2.0 -I${libdir}/glib/include
         </programlisting>
      </example>

      <para>Most of this should not be too surprising for people used to
      compiling programs: specify the required libraries, the linking and C
      flags for this package and anything it conflicts with. For more example,
      look in the standard location where <application>pkg-config</application>
      stored the <filename>*.pc</filename> files (usually
      <filename>$(prefix)/lib/pkgconfig/</filename>.</para>

   </sect2>

   <sect2 id="configure.in-changes"
          xreflabel="required changes to configure.in">
      <title>Changes to <filename>configure.in</filename></title>

      <para>The <filename>configure.in</filename> file is probably the one that
      requires the most changes from amongst those that are part of the
      application build infrastructure. There are a variety of reasons for
      this.</para>
      
      <para>For GNOME 2, the need for every CVS module to have a
      <filename>macros/</filename> subdirectory containing a whole group of
      common <filename>.m4</filename> macros has been removed. Instead, the
      common macros are part of the <application>gnome-common</application>
      module and will normally be available (when installed) under
      <filename>$(prefix)/share/aclocal/gnome2-macros</filename>. </para>

      <para>Similarly, there is generally little need to use the
      <application>gettext</application> from the <filename>intl/</filename>
      directory that is in most modules, since
      <application>gettext</application> is generally already installed on the
      developer's machine.</para>

      <para>Finally, some changes are required due to the upgrading of various
      tools. For example, some are recommended as part of the move to
      <application>autoconf 2.52</application>. These allow better diagnostic
      messages, for the most part. The inclusion of
      <application>pkg-config</application> will also require the addition of a
      few calls.</para> 

      <para>Of course, as with all of the suggestions throughout this document,
      if you need something more complex than the above, you already understand
      why you need something different and know enough to change it. This
      document simply provides a checklist for the majority of applications in
      this document; it won't necessarily work for some really tricky corner
      cases.</para>

      <para>So now that the justifications are out of the way, here are the
      steps that will work in most cases:</para>

      <orderedlist>
         <listitem>
            <para>Remove any call to the <systemitem
            class="macro">GNOME_COMMON_INIT</systemitem> macro. This has been
            obsoleted by the way <filename>gnome-autogen.sh</filename> sets up
            the environment.  </para>
         </listitem>
         <listitem>
            <para>In <application>autoconf 2.52</application>, the
            <systemitem class="macro">AC_INIT</systemitem> macro now takes two
            parameters, plus one optional one. The parameters are
            <replaceable>package-name</replaceable>,
            <replaceable>version-number</replaceable> and
            <replaceable>bug-address</replaceable>, with the last one being
            optional.</para>

            <para>For GNOME 1, your <filename>configure.in</filename> file
            would have had a line that said something like
               <programlisting>
AC_INIT(foo.c)
AM_INIT_AUTOMAKE(foo, 1.23)
               </programlisting>
            For GNOME 2, this should be altered to be
               <programlisting>
AC_INIT(foo, 1.23, http://bugzilla.gnome.org/enter_bug.cgi?product=foo)
AC_CONFIG_SRCDIR(foo.c)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
               </programlisting>
            Note that <systemitem class="macro">AM_INIT_AUTOMAKE</systemitem>
            takes advantage of the fact that <systemitem
            class="macro">AC_INIT()</systemitem> now defines the
            <varname>AC_PACKAGE_NAME</varname> and
            <varname>AC_PACKAGE_VERSION</varname> variables.</para>
         </listitem>
         <listitem>
            <para>Including the relevant macros for
            <application>pkg-config</application> is relatively straightforward
            once you determined the top of you dependency tree from amongst the
            platform libraries (see the earlier <xref linkend="pkg-config"
            endterm="pkg-config-title"/> section for more information about
            this). In what follows, we assume the package you building is
            called <application>foo</application>.</para>

            <para><anchor id="pkg-config-configure.in" xreflabel="pkg-config
            usage"/>To collect the require C flags and library linking flags,
            use the following sequence of macros.
               <programlisting>
PKG_CHECK_MODULES(FOO, libgnomeui-2.0)
AC_SUBST(FOO_CFLAGS)
AC_SUBST(FOO_LIBS)
               </programlisting>
            Then your <filename>Makefile.am</filename> can refer to the
            <varname>$(FOO_CFLAGS)</varname> and <varname>$(FOO_LIBS)</varname>
            variables in the <varname>INCLUDES</varname> and
            <varname>LDADD</varname> assignment lines respectively.</para>

            <para>In the unusual case where you might require particular
            minimum versions of some libraries (for example, to avoid a bug in
            an eariler version), you can do something like the following
            extract.</para>
            
            <programlisting>
dnl It is generally 'good practice' to put pkg-config requirements
dnl at the start so versions can be changed easier at a later date
GTK_REQUIRED=1.3.1
LIBGNOMEUI_REQUIRED=1.96.0

PKG_CHECK_MODULES(FOO, gtk+-2.0 &gt;= $GTK_REQUIRED
                       libgnomeui-2.0 &gt;= $LIBGNOMEUI_REQUIRED)
AC_SUBST(FOO_CFLAGS)
AC_SUBST(FOO_LIBS)
            </programlisting>
         </listitem>
         <listitem>
            <para>Finally, in order to use a locally installed version of
            <application>gettext</application>, it is recommended to use
            <systemitem class="macro">AM_GLIB_GNU_GETTEXT</systemitem>. This
            replaces any call to <systemitem
            class="macro">AM_GNOME_GETTEXT</systemitem> or even <systemitem
            class="macro">AM_GNOME2_GETTEXT</systemitem>. </para>

            <para>You will also need to define the
            <constant>GETTEXT_PACKAGE</constant> constant using something like
            the following.
               <programlisting>
GETTEXT_PACKAGE=foo
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE")
               </programlisting>
            Note that if your application is meant to be installed in parallel
            with its GNOME 1 counterpart, then you should write
            <literal>GETTEXT_PACKAGE=foo-2.0</literal>. In either case,
            whenever you call <application>gettext</application> functions like
            <literal>bindtextdomain()</literal> or
            <literal>bind_textdomain_codeset()</literal> in your code, you
            should use <constant>GETTEXT_PACKAGE</constant> instead of the
            literal package name as the first parameter to these functions.
            </para>
         </listitem>
         <listitem>
            <para>Remove all references to <filename>intl/Makefile</filename>,
            <filename>macros/Makefile</filename>. These are now no longer
            needed, due to the earlier changes.</para>
         </listitem>
         <listitem>
            <para>If you are generating signal marshallers for use with
            <application>gtk</application>, you may need to determine the
            location of <command>glib-genmarshal</command>. The
            <filename>configure.in</filename> change required to do this is
            described in the section about <xref linkend="gtk-marshallers"
            endterm="gtk-marshallers-title"/>. </para>
         </listitem>
      </orderedlist>
   </sect2>

   <sect2>
      <title>Changes to <filename>Makefile.am</filename></title>

      <para>Usually, you will not need to make substantive changes to your
      <filename>Makefile.am</filename>, since it already just contains the
      minimal amount of information needed for you application to build. The
      main thing you should do is remove any references to the
      <filename>intl</filename> and <filename>macros</filename> directories in
      targets like <varname>DIST_SUBDIRS</varname> and
      <varname>SUBDIRS</varname>. </para>

      <para>In the section on <xref linkend="scrollkeeper"
      endterm="scrollkeeper-title"/> we will discuss some
      <filename>Makefile.am</filename> changes that are required for adding
      <application>Scrollkeeper</application> support to your document.
      Similarly, in the section on <xref linkend="gtk-marshallers"
      endterm="gtk-marshallers-title"/> we discuss some
      <filename>Makefile.am</filename> changes that are required if you need to
      generate signal marshallers in <application>gtk</application>. </para>

   </sect2>
</sect1>

