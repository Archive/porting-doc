<!-- vim: set ai et sw=3 ts=3 tw=80: -->
<sect1 id="session-management">
   <sect1info>
      <authorgroup>
         <author>
            <surname>Pennington</surname>
            <firstname>Havoc</firstname> 
            <affiliation>
               <address><email>hp@redhat.com</email></address>
            </affiliation>
         </author>
      </authorgroup>
   </sect1info>
   <title id="session-management-title">Session management</title>

   <para> The concept of session management hasn't changed much in the
   move from GNOME 1 to GNOME 2. Applications should still use the
   <filename>&lt;libgnomeui/gnome-client.h&gt;</filename> interface to
   make their applications session aware. It's important to be clear
   about what parts of application state should be saved per-session, 
   and which should be stored as preferences. This document tries to 
   clarify that issue.
   </para>

   <sect2 id="session-management-overview">
      <title>Preferences and Session Management Overview</title>

      <para>
        Applications typically have <firstterm>preferences</firstterm> 
        which are global and apply to all instances of the
        application. Ideally, changing a preference in one instance 
        of the application will automatically change the same preference in all
        other instances. GConf makes this happen automatically.
      </para>

      <para>
         A <firstterm>session</firstterm> is a collection of
         application instances, possibly defined under some
         user-provided name. For example, the user might have "Home"
         and "Work" sessions. The set of application instances can be
         saved to disk, and then later restored. Sessions are saved
         and restored by a special program called the
         <firstterm>session manager</firstterm>. Each application
         instance provides the session manager with enough information to
         restart that application instance in its current state.
      </para>

      <para>
         Sessions are saved by taking "snapshots" of a group of
         applications connected to the session manager. For each
         snapshot, the session manager sends a request called a
         "SaveYourself" to all application instances. Each instance
         then provides the session manager with a command line that,
         when executed, will result in an application instance with
         the same state as the currently-open instance,
         <emphasis>and</emphasis> the same <firstterm>session client
         ID</firstterm>.
                  <footnote>
                     <para> The application session ID is a globally unique
                     Latin-1 encoded string identifying an application
                     instance.  Applications receive their ID from the session
                     manager by calling
                     <literal>gnome_client_get_id()</literal>. This ID may be
                     <literal>NULL</literal> if not connected to the session
                     manager.</para>
                  </footnote>
         For example, 
         a terminal program with three windows open might give 
         the session manager the command line <literal>terminal
         --with-window-count=3 --session-client-id=2341341534</literal>, or
         something along those lines.
      </para>

      <para>
        Note that each "SaveYourself" is independent of the others. 
        The same application instance (with the same session ID) may
        be asked to save multiple times in the same session. Each save
        must continue to work &mdash; apps do not know which one of the
        saves will be used by the session manager. As a result, if 
        applications save persistent state, they should namespace that 
        persistent state with a per-save unique key. Unfortunately, 
        gnome-libs has no way to generate such a key; one simple
        method might be:
<programlisting>
 key = g_strdup_printf ("%d-%d-%u",
                        (int) getpid (),
                        (int) time (),
                        g_random_int ());                          
</programlisting>
        The application would then use this key as part of the configuration
        key used to store per-save data. For example, in GConf you might store
        per-save data under
        <literal>/apps/<replaceable>application_name</replaceable>/sessions/<replaceable>save_key</replaceable></literal>
        where <replaceable>save_key</replaceable> is the per-save key. </para>

      <para>
       Note that saving per-save data in a persistent location
       requires you to provide a "discard command" to the session
       manager, so it can clean up your persistent data 
       when it throws out the saved snapshot in question. 
       This can all be kind of a pain, so apps are encouraged to 
       save all their state as part of the command they pass to the
       session manager, and avoid saving persistent files.  That is, it's
       easier and more robust to have a command line option
       <parameter>--with-window-count=3</parameter> than it is to store the
       window count in GConf under
       <literal>/apps/terminal/sessions/4543-3252345-6745/window_count</literal>
       and provide a discard command to clear that key.  </para>

      <note>
         <para> Sometimes GNOME has a piece of state that should be
         per-session, but belongs to the entire desktop.  For example, say you
         wanted per-session background images.  We thought we had a plan here
         with <literal>gnome_client_get_desktop_id()</literal>, but we were
         wrong. We don't have a plan yet. We are trying to come up with one.
         </para>
      </note>

   </sect2>

   <sect2 id="session-where-to-put-data">
      <title>Where to store data?</title>
      
      <para> By following the rules below it should be relatively easy to
      determine how to give your application the correct session support.
      </para>

      <para>
         <itemizedlist>
            <listitem>
               <para>Only settings related to application <emphasis> instance
               </emphasis> state should be stored when the session manager asks
               you to save. Examples are the currently-open windows, and what
               documents are opened in the windows. Most settings are global
               preferences and not session state. Session state is ideally
               simple enough to encode in the command line used to launch your
               application. </para>
            </listitem>

            <listitem> 
               <para>The window manager stores window positions. Application
               developers need not bother to do so. </para>
            </listitem>

            <listitem>
               <para>Important or critical data, including preferences that
               take effort to set up, should never be stored per-session. If a
               user spends a lot of time setting up their panel or terminal,
               they do not want to lose that data when changing sessions, or if
               something goes wrong with the session system.  Data such as
               documents should <emphasis>NEVER</emphasis> go in the
               instance-specific location. </para>
            </listitem>

            <listitem>
               <para>User preferences should almost always apply globally to
               all application instances in all sessions. With
               <application>gconf</application> this can happen without even
               restarting the application instance. </para>
            </listitem>
            
            <listitem>
               <para><emphasis>(FIXME: We have to figure out how to handle
               desktop-wide per-session info.)</emphasis>
               </para>
            </listitem>

            <listitem>
               <para>In no case should the same setting be stored both
               per-instance and globally. Let's repeat that, to avoid some
               disasters that happened in GNOME 1: <emphasis>DO NOT STORE THE
               SAME SETTING BOTH PER-INSTANCE AND GLOBALLY.</emphasis>
               </para>
            </listitem>
            
            <listitem>
               <para>Per-session settings should not appear in any preferences
               dialogs, because it's confusing. Preferences dialogs should
               affect the global state of the application. Anything
               per-instance should be in a menu or something, not in the
               preferences dialog. </para>
            </listitem>
         </itemizedlist>
      </para>

   </sect2>

   <sect2 id="session-implementation-notes">
      <title>Implementation Notes</title>

      <para> Often you want to allow per-session setups, but setting up an
      application involves noticeable user effort. One suggested solution to
      this problem is to have a "named profile" feature in the application. Make
      the list of profiles, and their contents, a global preference making it
      easy to restore the activate profile when necessary. The active profile
      can be a per-session or per-instance setting. Using profiles is
      complicated so most applications should not need to use them.
         <footnote>
            <para>A profile is a named set of application settings. e.g. the
            'terminal class' of
            <application>gnome-terminal</application>. This makes
            sense when an application can be used in multiple contexts
            by the same user, and that user may want different
            settings in each context. </para>
         </footnote>
      </para>

      <para> Applications <emphasis>must always</emphasis> open the same number
      of windows they had open at session save time. If you have not implemented
      this, then please <emphasis>do not</emphasis> connect to the session
      manager because you will confuse the window manager when it tries to
      position missing windows. Use
      <literal>gnome_disable_master_client()</literal> until you have
      implemented this.
     </para>

      <para> When setting the <literal>RestartCommand</literal> for your
      application, use the standard
      <literal>--sm-client-id</literal> option to pass the current
      session ID to the restarted application. </para>

      <para> If you store session state in <application>gconf</application>, the
      <literal>DiscardCommand</literal> can use the
      <literal>--recursive-unset</literal> option to
      <application>gconftool</application>. </para>
   </sect2>

   <sect2 id="session-reference">
      <title>References</title>

      <para>
         <itemizedlist>
            <listitem>
               <para> <ulink
               url="http://mail.gnome.org/archives/gnome-hackers/2001-November/msg00003.html">Session
               Management for GNOME 2.0</ulink> - Havoc Pennington
               2001-11-01 (Note one important mistake in the original
               post, which is that session state should be
               per-session-save, not per-session-ID - in other words, do
               <emphasis>not</emphasis> use the session ID to key your saved
               session data.) </para>
            </listitem>
         </itemizedlist>
      </para>

   </sect2>

</sect1>
