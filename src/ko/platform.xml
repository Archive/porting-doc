<!-- vim: set ai et sw=3 ts=3 tw=80: -->
<sect1 id="platform">
   <title id="platform-title">The platform libraries</title>

   <para>GNOME 2 consists of a group of platform libraries, some non-platform
   libraries and a whole pile of applications that depend on the previous two
   groups.</para>

   <para>It is probably necessary to clarify the difference between a
   <emphasis>platform</emphasis> and <emphasis>non-platform</emphasis>
   library, since we will really be discussing platform libraries in the
   sections that follow. <ulink
   url="http://mail.gnome.org/archives/gnome-2-0-list/2001-September/msg00141.html">Some
   mail</ulink> from Maciej Stachowiak to the gnome-2-0 mailing list probably
   explains this most succintly:</para>

   <blockquote>
      <para>A "platform library" is one we commit to support as part of the
      platform and recommend to internal developers. Some aspects of this
      include committing to total source and binary compatibility within a
      major GNOME platform version, and licensing suitable for use by both free
      software and proprietary software
         <footnote>
            <para>All of the current platform libraries are licensed under the
            LGPL.</para>
         </footnote>
      .</para>

      <para>But even in the case of libraries whose maintainers are willing to
      meet these criteria, we may not want to include them if we don't feel
      they are mature enough, if they duplicate functionality already in the
      platform, if they do not interoperate well with other parts of the
      platform, etc.</para>
   </blockquote>

   <para>Currently, the following libraries are part of the GNOME 2
   platform. The order of this list is one possible build order that works (see
   <xref linkend="package-downloading" endterm="package-downloading-title"/>
   for more info).</para>

   <table frame="all">
      <title>GNOME 2 platform library modules</title>
      <tgroup cols="3">
         <tbody>
            <row>
               <entry><para>intltool</para></entry>
               <entry><para>gnome-common</para></entry>
               <entry><para>esound
                     <footnote>
                        <para> This library is just carried over from the
                        GNOME 1 platform with no change.</para>
                     </footnote>
                  </para>
               </entry>
            </row>
            <row>
               <entry><para>glib</para></entry>
               <entry><para>pango</para></entry>
               <entry><para>atk</para></entry>
            </row>
            <row>
               <entry><para>gtk+</para></entry>
               <entry><para>libxml2
                     <footnote>
                        <para>In the GNOME CVS repository, this is called
                        <application>gnome-xml</application>.</para>
                     </footnote>
                  </para>
               </entry>
               <entry><para>linc</para></entry>
            </row>
            <row>
               <entry><para>libIDL</para></entry>
               <entry><para>ORBit2</para></entry>
               <entry><para>bonobo-activation</para></entry>
            </row>
            <row>
               <entry><para>gconf</para></entry>
               <entry><para>libbonobo</para></entry>
               <entry><para>gnome-vfs</para></entry>
            </row>
            <row>
               <entry><para>libart_lgpl</para></entry>
               <entry><para>bonobo-config</para></entry>
               <entry><para>libgnome</para></entry>
            </row>
            <row>
               <entry><para>libgnomecanvas</para></entry>
               <entry><para>libbonoboui</para></entry>
               <entry><para>libgnomeui</para></entry>
            </row>
            <row>
               <entry><para>libglade</para></entry>
               <entry><para>gail</para></entry>
               <entry><para>libgnomeprint</para></entry>
            </row>
            <row>
               <entry><para>libgnomeprintui</para></entry>
               <entry><para>libxslt</para></entry>
               <entry></entry>
            </row>
         </tbody>
      </tgroup>
   </table>
</sect1>

