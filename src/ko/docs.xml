<!-- vim: set ai et sw=3 ts=3 tw=80: -->
<sect1 id="docs">
   <title id="docs-title">Documentation</title>

   <para>Hopefully your application already has lots of documentation; possibly
   in a few different languages. Preparing the documentation for GNOME 2 is a
   very simple process. There are really only two major changes and one
   cosmetic alteration from the GNOME 1 setup.</para>

   <sect2 id="docbook-4-1">
      <title>Documentation markup</title>

      <para>The GNOME 2 platform will be using DocBook-XML version 4.1.x markup
      for all of its documents. In GNOME 1, DocBook version 3.1 was used and
      the markup was in SGML format. The changes required to move to version
      4.1 are relatively simple.</para>

      <orderedlist>
         <listitem>
            <para>Since the document must now be a valid XML document, insert a
            line saying <markup role="xml">&lt;?xml version="1.0"
            standalone="no"?&gt;</markup> as the first line of each main
            document.</para>
         </listitem>
         <listitem>
            <para>Replace the <markup role="xml">DOCTYPE</markup> declaration
            with a line saying <markup role="xml">&lt;!DOCTYPE article PUBLIC
            "-//OASIS//DTD DocBook XML V4.1.2//EN"
            "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd"&gt;
            </markup>.</para>

            <para>Of course, you can replace the <markup
            role="xml">article</markup> reference with <markup
            role="xml">book</markup> or <markup role="xml">chapter</markup> as
            appropriate. It is important to get the <literal>DOCTYPE</literal>
            declaration <emphasis>exactly</emphasis> right, since otherwise you
            and your users will not be able to take advantage of the effect of
            catalogs and even external resolution of the DTD may fail.</para>
         </listitem>
         <listitem>
            <para>Note that XML is case-sensitive (SGML, in its most general
            form, is not). So all of the DocBook tags used in your documents
            should be in lowercase.  There are a number of other small
            adjustments that may need to be made, depending upon how much the
            original documents relied upon SGML features that are not found in
            XML. Refer to <ulink
            url="http://www.docbook.org/tdg/en/html/appb.html">Appendix
            B</ulink> of the DocBook book for information on these. </para>

            <para>In most cases, however, it will be sufficient to simply
            proceed to the next step and correct errors as they arise.  </para>
         </listitem>
         <listitem>
            <para>Ensure that <application>xmllint</application> from the
            <application>libxml2</application> package is in your
            <envar>PATH</envar>. Supposing your document is called
            <filename>foo-doc.xml</filename>, run the command
            <command>xmllint --valid --noent --noout foo-doc.xml</command> to
            ensure it is valid DocBook XML markup. </para>

            <note>
               <para>The <parameter>--valid</parameter> parameter to
               <command>xmllint</command> will attempt to download the DocBook
               DTD from the Internet. If you are not connected to an external
               network, or would prefer not to use up the bandwidth each time,
               it is possible to use the <parameter>--catalogs</parameter> to
               verify against a local copy of the DTD. Alternatively,
               <application>xmllint</application> will use the file
               <filename>/etc/xml/catalog</filename>, if it exists, to
               determine what local files to use.</para>

               <para>For more information on this, have a look at <ulink
               url="http://www.xmlsoft.org/catalog.html"/>, where there is a
               good introduction to catalogs and some links to other resources
               (including a link to the <ulink
               url="http://www.oasis-open.org/committees/entity/spec-2001-08-06.html">
               draft standard</ulink> for XML catalogs). Also, you might want
               to look at the <xref linkend="producing-html"
               endterm="producing-html-title"/> section in the appendices,
               where I describe how I set this up for myself.</para>
            </note>

            <para>If there are any errors in the document,
            <application>xmllint</application> will print the filename and the
            line number and a brief description of the problem. Fix each error
            in turn, from the beginning (since a single early problem can cause
            many cascading failures further along the parsing process) and
            rerun <application>xmllint</application> until all the problems
            have gone away.</para>

            <para>Note that from time to time, you will get some very strange
            looking messages from <application>xmllint</application> that seem
            to be saying some tag is too long, but the piece of context it
            displays in the error message looks perfect. This is almost always
            because you forgot to close a tag many lines earlier. Often this is
            a consequence of converting a document from SGML format (where
            closing tags are sometimes optional) to XML format (where the
            closing tags are mandatory). There is not currently any easy way to
            find the exact tag that is causing the problem here, except for
            careful searching. </para>
         </listitem>
      </orderedlist>

      <sect3 id="checking-docs">
         <title>Checking to see that your documents look alright when converted
         to HTML</title>

         <para>Although GNOME 2 will ship almost all of its documents in raw
         XML format and turn them into HTML as required, you will from time to
         time want to check that your document looks sensible when marked up.
         Eventually, there will be an "approved" way of doing this, but for
         right now I have included a description of what to do to turn
         <emphasis>this</emphasis> document into html in the <xref
         linkend="contributing" endterm="contributing-title"/> appendix (see
         the <xref linkend="producing-html" endterm="producing-html-title"/>
         section). </para>
      </sect3>

   </sect2>

   <sect2 id="scrollkeeper">
      <title id="scrollkeeper-title">Scrollkeeper support</title>

      <para><emphasis>(Parts of this section are based on notes from John Fleck
      about building with <application>Scrollkeeper</application>.)</emphasis>
      </para>

      <para><application>Scrollkeeper</application> is an application for
      managing document metadata and provides the infrastructure necessary to
      allow for easy searching and retrieval of documents known to
      <application>Scrollkeeper</application>. </para>
      
      <para>The metadata is stored in a manner specified by the Open Source
      Metadata Framework (<acronym>OMF</acronym>). More information about the
      <acronym>OMF</acronym> is available from <ulink
      url="http://www.ibiblio.org/osrt/omf/"/>, while information about
      <application>Scrollkeeper</application> itself can be found at <ulink
      url="http://scrollkeeper.sourceforge.net/"/>. </para>

      <para>Adjusting your application to provide information about its
      documentation to <application>Scrollkeeper</application> involves the
      following steps and we will discuss each one in turn in the sections
      that follow.
         <itemizedlist>
            <listitem>
               <para>Creating a <filename>*.omf</filename> files which contains
               the OMF data for your documentation. </para>
            </listitem>
            <listitem>
               <para>Create an appropriate <filename>Makefile.am</filename> for
               your documentation. </para>
            </listitem>
            <listitem>
               <para>Install the OMF files in the correct location and inform
               <application>Scrollkeeper</application> of their existence.
               </para>
            </listitem>
         </itemizedlist>
      </para>

      <para>To get a quick idea of what is involved, you may wish to have a
      look at the <filename>gnome-docu/gdp/gdp-example1</filename> directory in
      the GNOME CVS repository, which gives an example of installing some
      documentation using <application>Scrollkeeper</application>. </para>

      <sect3 id="create-omf">
         <title>Creating OMF files</title>

         <para>Each translation of each document will need an OMF file created
         for it. Traditionally
            <footnote>
               <para>OK... there is not a <emphasis>lot</emphasis> of tradition
               in something this new, but you could be thought of as a
               visionary if you adopt this scheme. </para>
            </footnote>
         these files have the same name as the main documentation file, with
         the filename extension (<filename>.sgml</filename> or
         <filename>.xml</filename>) removed and the language code added on
         together with <filename>.omf</filename>. So, for example, if your
         documentation file is called <filename>wonder-doc.xml</filename> and
         you were writing the OMF file for the German translation, you would
         call the file <filename>wonder-doc-de.omf</filename>. </para>

         <para>The OMF files are valid XML documents, containing information
         about the creators of the document, its title, a brief description of
         it, where it fits in to the <application>Scrollkeeper</application>
         hierarchy of documents and so on. The 16 different elements that are
         available for use can be seen at <ulink
         url="http://www.ibiblio.org/osrt/omf/omf_elements"/>, while the full
         DTD for OMF files is available at <ulink
         url="http://www.ibiblio.org/pub/Linux/metadata/OMF.dtd"/>. </para>

         <para>To create an OMF file, you can either take an existing file and
         modify it appropriately. For example, the file in the GNOME
         CVS repository under
         <filename>gnome-docu/gdp/gdp-example1/help/gdp-example1-manual/C/gdp-example1-manual-C.omf</filename>
         is a good model. The alternative is to realize that most of the
         information you need is probably already in your main document (or can
         at least be stored there). Martijn van Beers came up with a useful
         XSLT document for extracting the required metadata from a valid XML
         document. His idea is available in the GNOME CVS repository under
         <filename>gnome-docu/gdp/tools/</filename> and includes a
         <filename>README</filename> file explaining what to put in your main
         document. With care, you can then adjust your build process to create
         the <filename>.omf</filename> file on the fly as required. </para>
      </sect3>

      <sect3 id="install-docs">
         <title>Installing the documentation</title>
         <para>Building and installing documentation is simplified in
         GNOME 2. In previous versions of GNOME, HTML had to be built at package
         build time to be installed and read by the help browser. In GNOME 2,
         the browser can render the help file directly from the DocBook
         XML, so installation of the DocBook file, along with registration of
         the documentation with ScrollKeeper, is all that is required.</para>

      <para>There is a docs build template in
      <filename>gnome-docu/gdp/gdp-example1/</filename> in GNOME cvs.</para>

      <para>A <filename>Makefile.am</filename> template called
      <filename>xmldocs.make</filename> has been written to simplify the docs
	build system. It can be found in the directory
	<filename>gnome-docu/gdp/gdp-example1/help</filename> in GNOME cvs. The
	<filename>Makefile.am</filename> in your docs directory then includes
	<filename>xmldocs.make</filename>, along with references to the
      location of any image files used as figures in your documents. An example
	can be found at
      <filename>gnome-docu/gdp/gdp-example1/help/gdp-example1-manual/C/Makefile.am</filename>
      in GNOME cvs.
      </para>
      <para>The examples above require creation of a directory called
      <filename>omf-install</filename> in your application's root
      directory. This is where the Makefile temporarily places a copy of the OMF
	file modified to reflect the final installation location. A
      <filename>Makefile.am</filename> also must be placed in this directory
	similar to the one found in
	<filename>gnome-docu/gdp/gdp-example1/omf-install</filename>. This
	<filename>Makefile.am</filename> handles the final installation chores.</para>
      </sect3>
   </sect2>

   <sect2>
      <title>Style guide and wordlist</title>
      <para>A new GNOME Documentation Style Guide has been developed to unify
      user documentation. It includes a standardized word list as well and tips
      to improve documentation usability. It is available at <ulink
      url="http://developer.gnome.org/documents/style-guide/"/>. </para>
   </sect2>
   <sect2>
      <title>GNOME Documentation Project</title>
      <para>Help on problems with writing, building or installing documentation
      is available from the <ulink
      url="http://mail.gnome.org/mailman/listinfo/gnome-doc-list">gnome-doc-list</ulink>
      or by visiting #docs at irc.gnome.org.</para>
   </sect2>
</sect1>

