<!-- vim: set ai et sw=3 ts=3 tw=80: -->
<sect1 id="gconf">
   <title id="gconf-title">GConf</title>

   <para><application>GConf</application> existed already for GNOME 1.4,
   however, to date very few applications have been using it, preferring
   instead to manipulate flat text files for their applications, as has been
   the "GNOME" and "Unix" way. For GNOME 2, it is recommended that as much
   configuration as possible be moved to being managed by
   <application>GConf</application>. </para>

   <para>Since <application>GConf</application> is going to be relatively new
   to most developers, I will not duplicate its excellent documentation here.
   The best source of documentation is the <ulink
   url="http://developer.gnome.org/doc/API/gconf/index.html">API manual</ulink>
   which can be found on the <ulink url="http://developer.gnome.org/">GNOME
   developer's</ulink>. That document also includes an introduction to
   <application>GConf</application>, so if you are a bit confused by how it
   works, there should be sufficient explanation in those documents to clarify
   the problems. Of course, there is also the <ulink
   url="http://mail.gnome.org/mailman/listinfo/gconf-list">gconf mailing
   list</ulink>, which is an excellent place to ask for help. You can browse an
   archive of this list at <ulink
   url="http://mail.gnome.org/archives/gconf-list/"/>. </para>

   <para>Havoc Pennington has also written an excellent introduction to 
   <application>GConf</application> which is available at <ulink
   url="http://developer.gnome.org/feature/archive/gconf/gconf.html"/> (or
   downloadable as a postscript document from <ulink
   url="http://developer.gnome.org/feature/archive/gconf/gconf.ps"/>. </para>

   <para>Conceptually, moving to <application>gconf</application>-based
   configuration management is fairly simple. You plan out what parameters you
   wish to configure that are specific to your application and design a schema
   for them. You may also wish to take advantage of system-wide or desktop-wide
   parameters. For example, the <literal>/desktop/gnome/interface/</literal>
   directory (a "directory" in the <application>gconf</application> hierarchy,
   not a literal filesystem directory) contains a number of settings that
   define what the user would like to use to view various items. Normally, you
   will not care what these settings are and will just call the appropriate
   function in <application>libgnome</application> or
   <application>libgnomeui</application> to do the presenting. However, it may
   be necessary to look at these values in your application and they are
   accessible if you need to (one example is the control-center application
   which will want to set these parameters). Should this sort of information be
   applicable to your application, you can see what settings are supplied by
   other libraries by looking in the <filename>*.schemas</filename> file that
   they install (usually in <literal>$(sysconfdir)/gconf/schemas/</literal>).
   </para>

</sect1>
